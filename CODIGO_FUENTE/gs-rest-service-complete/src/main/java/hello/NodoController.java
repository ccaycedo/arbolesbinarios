package hello;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import logica.Entrada;
import logica.ListaDoble;

@RestController
public class NodoController {
	
	//declaro la clase principal de arboles binarios
	public ListaDoble lista=new ListaDoble();

	// servicio para crear un nodo
	@RequestMapping("/crearNodo")
	@PostMapping    
	public String crearNodo(@RequestBody Entrada entrada) {
				
		lista.insertarAntesDe(entrada.padre, entrada.izquierda);
		lista.insertarDespuesDe(entrada.padre, entrada.derecha);
		
		return lista.despliegaLista();
    }
	
	// servicio para crear el arbol unicamente creo el nodo principal
	@RequestMapping("/crearRaiz")
	@PostMapping   
	public String crearRaiz(@RequestBody Entrada entrada) {
		
		// creo el nodo raiz
		lista.insertarAlFrente(entrada.raiz);				
		return "Se creo el nodo raiz";
	
	}
	
	// servicio para buscar el ancestro comun
	@RequestMapping("/buscarAncestro")
	@PostMapping    
	public String buscarAncestro(@RequestBody Entrada entrada) {
		
		//busco el ancestro comun
		lista.ancestroComun=0;
		return "El ancestro comun es "+lista.buscarAncestro(entrada.nodoA, entrada.nodoB);
	
	}

}
