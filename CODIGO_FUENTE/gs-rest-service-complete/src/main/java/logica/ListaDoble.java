package logica;

import java.util.Hashtable;

public class ListaDoble 
{
	/********************************************************/
	/********************************************************/
	/********************************************************/
	public NodoListaDoble Primero,Ultimo,Nuevo,Aux,Pos,Ant;
	public Hashtable<Integer, Integer> padresHijos=new Hashtable<>(); 
	int raiz=0;
	public int ancestroComun=0;
	/********************************************************/
	/********************************************************/
	/********************************************************/
	//////constructor
	public ListaDoble()
	{
		Primero = Ultimo = Nuevo = Aux = Pos = Ant = null;
	}
	/********************************************************/
	/********************************************************/
	/********************************************************/
	////////////////////////INSERTAR AL FRENTE//////////
	public void insertarAlFrente(int dato)
	{
		if(Primero==null){//////////1 caso(lista vacia)
			Primero = new NodoListaDoble(dato);
			Ultimo = Primero;
		}
		else{
			Nuevo = new NodoListaDoble(Ultimo, dato);
			Ultimo.Der = Nuevo;
			Ultimo = Nuevo;
		}
		raiz=dato;
		despliegaLista();
	}
	/********************************************************/
	/********************************************************/
	/********************************************************/
	////////////////INSERTAR ATRAS//////////////
	public void insertarAtras(int dato)
	{
		if(Primero==null){
			Primero = new NodoListaDoble(dato);
			Ultimo = Primero;
		}
		else {
			Nuevo = new NodoListaDoble(dato,Primero);
			Primero.Izq=Nuevo;
			Primero = Nuevo;
		}despliegaLista();
	}
	/////////INSERTAR ANTES DE //////////
	/********************************************************/
	/********************************************************/
	/********************************************************/
	public void insertarAntesDe(int datoB,int datoI)
	{
		if(Primero==null){
			//JOptionPane.showMessageDialog (null,"lista vacia");
			System.out.println("Lista vacia");
		}
		else{/////hay datos
			if(buscar(datoB)== true){
				///////EMPEZAR A REALIZAR EL METODO
				if(Aux==Primero){///caso 1
					Nuevo= new NodoListaDoble(datoI,Primero);
					Primero.Izq = Nuevo;
					Primero=Nuevo;
				}
				else{//en caso de que no este al inicio de la lista
					Nuevo = new NodoListaDoble(Ant,datoI , Aux);
					Aux.Izq = Nuevo;
					Ant.Der=Nuevo;
				}
				
				//coloco el hijo y el padre
				padresHijos.put(datoI, datoB);
			}    
		}
		despliegaLista();
	}
	/********************************************************/
	/********************************************************/
	/********************************************************/
	////////////////INSERTAR     DESPUES DE///////////////
	public void insertarDespuesDe(int datoB, int datoI)
	{
		if(Primero==null){
			//JOptionPane.showMessageDialog (null,"lista vacia");
			System.out.println("Lista vacia");
		}
		else{///hay metodos
			if(buscar(datoB)== true){
				//////EMPEZAR A REALIZAR EL METODO/////
				if(Aux==Ultimo){
					Nuevo=new NodoListaDoble(Aux,datoI);
					Aux.Der=Nuevo;
					Ultimo=Nuevo;
				}
				else{///en caso de que no este al inicio de la lista
					Nuevo= new NodoListaDoble(Aux,datoI,Aux.Der);
					Aux.Der = Nuevo;
					Pos = Aux.Der;
					Pos.Izq =Aux.Der;
				}
				
				//coloco el hijo y el padre
				padresHijos.put(datoI, datoB);

			}
		}despliegaLista();
	}
	/********************************************************/
	/********************************************************/
	/********************************************************/
	///////////////METODO ELIMINAR NODO//////////////
	public void eliminarNodo(int datoB)
	{
		if(Primero==null){
			//JOptionPane.showMessageDialog (null,"lista vacia");
			System.out.println("Lista vacia");
		}
		else{
			if(buscar(datoB)==true){///hacer cuatro casos
				if(Primero==Ultimo){//// 1 caso
					Primero=Ultimo=null;
				}
				else if(Aux==Primero){//2caso
					Primero=Aux.Der;
					Primero.Izq=Aux=null;
				}
				else if(Aux==Ultimo){//3 caso
					Ultimo=Ant;
					Ultimo.Der=Aux=null;
				}
				else{//4 caso
					Ant.Der=Pos;
					Pos.Izq=Ant;
					Aux=null;
				}
			}
		}despliegaLista();
	}
	/********************************************************/
	/********************************************************/
	/********************************************************/
	/////////////METODO PARA SUSTITUIR DATO///////////////
	public void modificaLista(int datoB,int datoI)
	{
		if(Primero==null){
			//JOptionPane.showMessageDialog (null,"lista vacia");
			System.out.println("Lista vacia");
		}
		else{
			if(buscar(datoB)==true){
				Aux.info=datoI;
			}
		}despliegaLista();
	}
	/********************************************************/
	/********************************************************/
	/********************************************************/
	//////////////////////////METODO BUSCAR/////////
	public boolean  buscar(int datoB)
	{
		Aux = Primero;
		boolean bandera = false;
		while (Aux != null && bandera != true) {
			if(datoB == Aux.info){// si encuentra el dato //la funcion .equals sirve para comparar el contenido de una direccion de memoria
				bandera = true;
			}
			else{//apunta al siguiente nodo
				Ant = Aux;
				Aux = Aux.Der;
				Pos = Aux.Der;
			}
		}
		if(bandera == true){
			return true;
		}
		else{
			//JOptionPane.showMessageDialog (null,"ese dato no existe");
			System.out.println("Lista vacia");
			return false;
		}
	}
	
	/********************************************************/
	/********************************************************/
	/********************************************************/
	public int buscarAncestro(int datoA,int datoB)
	{
		try {
			
			int padreA= raiz;
			int padreB=raiz;
			
			if(datoA != raiz) {
				padreA=padresHijos.get(datoA);
			}
			
			if(datoB != raiz) {
				padreB=padresHijos.get(datoB);
			}
			
			if(padreA == padreB) {
				ancestroComun=padreA;	
			}else if(padreB == datoA) {
				ancestroComun=padreB;
			}else if(padreA == datoB) {
				ancestroComun=padreA;
			}else {
				buscarAncestro(padreA,padreB);
			}
			
		} catch (Exception e) {
			return 0;
		}
		
		return ancestroComun;
		
		
	}
	
	/********************************************************/
	/********************************************************/
	/********************************************************/
	////////////////////DESPLEGAR LISTA DOBLE////////////////
	public String despliegaLista()
	{
		String lista = "";
		Aux = Primero;
		lista = "#########   LISTA COMPLETA   ########### \n\n";
		while (Aux != null) 
		{
			lista += "Dato = " + Aux.info; 
			lista += "\n";
			Aux = Aux.Der;
		}
		lista += "########################################";
		
		
		System.out.println("########################################");
		System.out.println("padres hijos");
		
		System.out.println(padresHijos);
		
		System.out.println(lista);
		
		return lista;
	}
	
	
	/********************************************************/
	/********************************************************/
	/********************************************************/
}