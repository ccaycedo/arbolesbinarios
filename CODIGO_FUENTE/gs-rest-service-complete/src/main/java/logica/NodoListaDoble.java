package logica;


public class NodoListaDoble 
{
    int info;
    NodoListaDoble Izq;/////////////asi se declara para apuntar a un dato igual a ellos
    NodoListaDoble Der;    
    
    /////////////primer constructor/*/////////////////
    public NodoListaDoble(int Dato){
        this.info = Dato;
        this.Izq = null;
        this.Der = null;
    }
    
    /////////////////segundo constructor///////////////
    public NodoListaDoble(NodoListaDoble Izq, int Dato){
        this.Izq = Izq;
        this.info = Dato;
        this.Der = null;
    }
    
    //////////////////tercer constructor//////////////
    public NodoListaDoble(int Dato, NodoListaDoble Der){
        this.Izq = null;
        this.info = Dato;
        this.Der = Der;
    }
    
    //////////////////cuarto constructor////////////////
    public  NodoListaDoble(NodoListaDoble Izq, int Dato, NodoListaDoble Der){
        this.Izq = Izq;
        this.info = Dato;
        this.Der = Der;
    }
}
