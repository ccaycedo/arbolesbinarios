# Ejercicio REST arboles binarios
Presentado por:

**Cristhian Alejandro Caycedo Torres**

Arquitecto de software

ccaycedo2@universidadean.edu.co

A continuación, se presenta la explicación y el desarrollo del ejercicio en API rest:

Requerimientos:
1) Crear un árbol binario.

![picture](img/arbolBinario.png)

2) Dado un árbol binario y dos nodos retornar el ancestro común más cercano.

## Estructura de carpetas
A continuación, se describe la estructura de carpetas del repositorio:
![picture](img/estructura.jpg)

## Explicación paso a paso

1) A continuación explico la estructura de paquetes del proyecto:

![picture](img/estructuraIde.png)

2) Arrancando el servidor de aplicaciones:

![picture](img/b.png)

3) Vamos a crear el siguiente árbol binario:

![picture](img/arbolBinario.png)

4) **PASO 1: Creamos la raíz del árbol en este caso el 67:**

![picture](img/r.png)

URL: http://localhost:8080/crearRaiz

METODO: POST

JSON: 

{
  "raiz":"67"
}

![picture](img/c.png)

4) PASO 2: Creamos el primer nodo:

![picture](img/d.png)

URL: http://localhost:8080/crearNodo

METODO: POST

JSON: 

{
  "padre":"67", 
  "izquierda":"39",
  "derecha":"76"
}

![picture](img/f.png)

4) PASO 3: Creamos el segundo nodo:

![picture](img/g.png)

URL: http://localhost:8080/crearNodo

METODO: POST

JSON: 

{
  "padre":"39", 
  "izquierda":"28",
  "derecha":"44"
}

![picture](img/h.png)

**Y de esta manera lo hacemos con los demás nodos como los queramos configurar.**

5) **PASO 4: Buscamos un ancestro común:**

![picture](img/i.png)

URL: http://localhost:8080/buscarAncestro

METODO: POST

JSON: 

{
  "nodoA":"83", 
  "nodoB":"87"
}

![picture](img/j.png)

6) Buscamos un ancestro común:

![picture](img/l.png)

URL: http://localhost:8080/buscarAncestro

METODO: POST

JSON: 

{
  "nodoA":"28", 
  "nodoB":"74"
}

![picture](img/m.png)

7) Buscamos un ancestro común:

![picture](img/o.png)

URL: http://localhost:8080/buscarAncestro

METODO: POST

JSON: 

{
  "nodoA":"74", 
  "nodoB":"87"
}

![picture](img/p.png)